﻿using System.Web.Mvc;

namespace AngSpa.UI.Controllers
{
    public class RootController : Controller
    {
        [Route("")]
        [HttpGet]
        public ViewResult Index()
        {
            return View();
        }
    }
}