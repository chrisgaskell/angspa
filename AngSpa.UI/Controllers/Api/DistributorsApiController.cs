﻿using System.Collections.Generic;
using System.Web.Http;
using AngSpa.UI.Models.Api;

namespace AngSpa.UI.Controllers.Api
{
    [RoutePrefix("api/distributors")]
    public class DistributorsApiController : ApiController
    {
        [Route("")]
        [HttpGet]
        public IEnumerable<Distributor> Get()
        {
            var distributors = new List<Distributor>();

            for (var i = 0; i < 2000; i++)
            {
                distributors.Add(new Distributor
                {
                    Id = i,
                    Name = $"Distibutor Name {i}",
                    Code = $"DHI{i}"
                });
            }

            return distributors;
        }

        // GET: api/Distributors/5
        [Route("{id:int}")]
        [HttpGet]
        public Distributor Get(int id)
        {
            return new Distributor {Id = 1, Name = "Distibutor Name"};
        }

        // POST: api/Distributors
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Distributors/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/Distributors/5
        [Route("{id:int}")]
        [HttpDelete]
        public void Delete(int id)
        {
        }
    }
}