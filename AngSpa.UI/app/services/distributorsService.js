﻿(function () {
    'use strict';

    var serviceId = 'distributorsService';
    angular.module('app').factory(serviceId, ['common', '$http', distributorsService]);

    function distributorsService(common, $http) {
        var $q = common.$q;

        var service = {
            getDistributors: getDistributors,
            deleteDistributor: deleteDistributor
        };

        return service;

        function getDistributors() {
            var distributors = $http.get('/api/distributors');
            return $q.when(distributors);
        }

        function deleteDistributor(id) {
            var request = $http({
                method: "DELETE",
                url: "/api/distributors/" + id
            });
            return $q.when(request);
        }
    }
})();