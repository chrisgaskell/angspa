﻿(function () {
    'use strict';
    var controllerId = 'distributors';
    angular.module('app').controller(controllerId, ['$scope', 'common', 'distributorsService', distributors ]);

    function distributors($scope, common, distributorsService) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.distributors = [];
        vm.title = 'Distributors';
        vm.distributorCount = 0;

        activate();

        function activate() {
            var promises = [getDistributors()];
            common.activateController(promises, controllerId)
                .then(function () { log('Loaded Distributors'); });
        }

        function getDistributors() {
            return distributorsService.getDistributors().then(function (response) {
                vm.distributorCount = response.data.length;
                return vm.distributors = response.data;
            });
        }

        $scope.deleteDistributor = function deleteDistributor(id, idx) {
            distributorsService.deleteDistributor(id).then(function (response) {
                vm.distributors.splice(idx, 1);
                vm.distributorCount = vm.distributors.length;
                log("Distributor deleted successfully");
            },
            function (error) {
                handleError(error);
            });
        }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message)
            {
                log("An unknown error occurred.");
            }

            // Otherwise, use expected error message. 
            return log(response.data.message);
        }
    }
})();